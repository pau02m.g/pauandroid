package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;


import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.ArrayList;


public class MyGdxGame extends ApplicationAdapter {
	//DEFINICIÓN VARIABLES
	int width; // ancho de la pantalla física.
	int height; // // alto de la pantalla física.
	OrthographicCamera cam; //La camera que usaremos.
	Jugador personaje; //sera nuestro personaje, esta clase contiene los métodos y atributos



	//necesarios para que lo podamos mover, rotar y escalar de una manera mas fácil.
	SpriteBatch batch; //la clase encargada de dibujar en pantalla.
	Texture textura; //variable donde almacenaremos nuestra textura.

	Esprite Slime;
	Esprite SlimeDos;
	Esprite SlimeTres;

	Texture texturaDos;
	Texture texturaTres;
	Texture texturaCuat;

	int anchopersona = 43;
	int altopersona = 64;

	int anchoSlime = 50;
	int altoSlime = 50;

	int paso = 0;
	int cambiopaso = 0;
	int sentido = 0;



	//FIN DEFINICIÓN VARIABLES

	private Music music;


	@Override
	public void create() {

		width = Gdx.graphics.getWidth();
		height = Gdx.graphics.getHeight();
		cam = new OrthographicCamera(width, height);
		cam.setToOrtho(false, width, height);
		batch = new SpriteBatch();


		textura = new Texture(Gdx.files.internal("caminando.png"));
		textura.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
		textura.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Linear);

		personaje = new Jugador(textura, anchopersona, altopersona, width);

		personaje.setRegion(paso * anchopersona, altopersona * sentido, (paso + 1) * anchopersona, altopersona * (sentido + 1));  // Definimos que parte de la tetura queremos mostrar,
		personaje.setPosition(10, 50);


		  //***********************//
		 //   nuevos personajes   //
		//***********************//


		texturaDos = new Texture("caminando.png");
		texturaDos.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
		texturaDos.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Linear);

		Slime = new Esprite(texturaDos, anchoSlime, altoSlime);
		Slime.setRegion(paso * anchoSlime, altoSlime * sentido, (paso + 1) * anchoSlime, altoSlime * (sentido + 1));  // Definimos que parte de la tetura queremos mostrar,
		Slime.setPosition(50, 400);


		///////////////

		texturaTres = new Texture("caminando.png");
		texturaTres.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
		texturaTres.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Linear);

		SlimeDos = new Esprite(texturaTres, anchoSlime, altoSlime);
		SlimeDos.setRegion(paso * anchoSlime, altoSlime * sentido, (paso + 1) * anchoSlime, altoSlime * (sentido + 1));  // Definimos que parte de la tetura queremos mostrar,
		SlimeDos.setPosition(200, 200);


		/////////////////////////////////


		texturaCuat = new Texture("caminando.png");
		texturaCuat.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
		texturaCuat.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Linear);

		SlimeTres = new Esprite(texturaCuat, anchoSlime, altoSlime);

		SlimeTres.setRegion(paso * anchoSlime, altoSlime * sentido, (paso + 1) * anchoSlime, altoSlime * (sentido + 1));  // Definimos que parte de la tetura queremos mostrar,
		SlimeTres.setPosition(500, 100);

		/*
		MUsica
		Creamos el objeto Music y le decimos que queremos que cuando acabe vuelva a empezar con el metodo setLooping pasando el valor true
		 */

		/*
		music = Gdx.audio.newMusic(Gdx.files.internal("sounds/fear.mp3"));
		music.setLooping(true);
		music.play();
		*/



	}

	@Override
	public void dispose() {

		batch.dispose();
		textura.dispose();


		/*  musica: Para liberar el fichero y no dejarlo abierto cuando cerremos la aplicacion se usa el metodo dispose */
		music.dispose();
	}

	@Override
	public void render() {

		// Logica juego

		// falsedad de movimento
		cambiopaso = (cambiopaso + 1) % 10;
		if (cambiopaso == 0) {
			paso = (paso + 1) % 6;
			personaje.setRegion(paso * anchopersona, altopersona * sentido, anchopersona, altopersona * (sentido + 1));  // Definimos que parte de la tetura queremos mostrar,
			Slime.setRegion(paso * anchopersona, altopersona * sentido, anchopersona, altopersona * (sentido + 1));
			SlimeDos.setRegion(paso * anchopersona, altopersona * sentido, anchopersona, altopersona * (sentido + 1));
			SlimeTres.setRegion(paso * anchopersona, altopersona * sentido, anchopersona, altopersona * (sentido + 1));


		}

		// Fin logica


		Gdx.gl20.glClearColor(1, 0, 1, 1);  // Definimos colo blanco de fondo
		Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT); //Limpiamos la pantalla

		batch.setProjectionMatrix(cam.combined); //Le asignamos al dibujador la camera que vamos a usar.
		// No hace falta asignarle camara y entonces usara por defecto es escenario.
		batch.begin(); //Antes de dibujar, tenemos que avisar al dibujador.

		//**********************************************
		// Dibujamos al personaje, pasando por parametro el dibujador
		//**********************************************

		personaje.draw(batch);
		Slime.draw(batch);
		SlimeDos.draw(batch);
		SlimeTres.draw(batch);

		personaje.MoverPersonaje();

		Slime.onColision(SlimeDos);
		Slime.onColision(SlimeTres);
		SlimeDos.onColision(SlimeTres);
		personaje.onColision(Slime);
		personaje.onColision(SlimeDos);
		personaje.onColision(SlimeTres);

		Slime.Revotes(width, height, 2);
		SlimeDos.Revotes(width, height,2);
		SlimeTres.Revotes(width, height, 2);



		batch.end(); //cerramos al dibujador

	}


	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

}
