package com.mygdx.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;




import java.lang.reflect.Array;

public class Esprite extends Sprite {

    private boolean PressedX;
    private boolean PressedY;
    private float ancho;
    private float alto;
    public Rectangle rec;
    private boolean toco;


    public Esprite(Texture texture, float ancho, float alto){
        super(texture);
        this.ancho = ancho;
        this.alto = alto;
        this.setSize(ancho, alto);

        rec = new Rectangle(this.getX(), this.getY(), this.getWidth(), this.getHeight());

        toco = false;
    }

    public void Revotes(int width, int height, int vel){

        // revote y tal

        // ancho
        if(this.getX() >= width - 40)
            this.PressedX = true;

        else if(this.getX() <= 0)
            this.PressedX = false;


        if(this.PressedX == false)
            this.setX(this.getX() + vel);

        else if (this.PressedX)
            this.setX(this.getX() - vel);



        //largo
        if(this.getY() >= height-50)
            this.PressedY = true;

        else if(this.getY() <= 0)
            this.PressedY = false;

        if(this.PressedY == false)
            this.setY(this.getY() + vel);

        else if (this.PressedY)
            this.setY(this.getY() - vel);


        this.rec.setX(this.getX());
        this.rec.setY(this.getY());
    }

    public void onColision(Esprite obj){
        float [] a = this.getVertices();
        float [] b = obj.getVertices();

        if(this.rec.overlaps(obj.rec)) {

            if((this.getX() > obj.getX() || this.getX() < obj.getX()) && toco) {
                this.setPressedX(!this.getPressedX());
                obj.setPressedX(!obj.getPressedX());


                this.toco = false;
            }
            else {
                this.setPressedY(!this.getPressedY());
                obj.setPressedY(!obj.getPressedY());
                this.toco = true;
            }

            /*

              if(a[6] >= b [1] || a[1] >= b[6]){
                this.setPressedY(!this.getPressedY());
                obj.setPressedY(!obj.getPressedY());
            }


            else if(a[5] >= b [0] || a[0] <= b[5]){
                this.setPressedX(!this.getPressedX());
                obj.setPressedX(!obj.getPressedX());
            }


             */
            /*
            this.PressedX = !this.PressedX;
            this.PressedY = !this.PressedY;
            obj.PressedX = !obj.PressedX;
            obj.PressedY = !obj.PressedY;
            */

        }
    }



    public float getAncho(){
        return this.ancho;
    }

    public float getAlto(){
        return this.alto;
    }

    public boolean getPressedX(){return this.PressedX;}

    public boolean getPressedY(){return this.PressedY;}

    public void setPressedX(boolean option){this.PressedX = option;}

    public void setPressedY(boolean option){this.PressedY = option;}


    public boolean getToco(){return this.toco;}

    public void setToco(boolean option){this.toco = option;}

}
