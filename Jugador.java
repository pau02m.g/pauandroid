package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;

public class Jugador extends Esprite{

    private int Pressed;
    private boolean ReverseMon;
    private int LimiteMapa;

    public Jugador(Texture texture, float ancho, float alto, int width){
        super(texture, ancho, alto);
        this.LimiteMapa = width;
    }

    public void MoverPersonaje(){
        if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)){
            //personaje.setX(personaje.getX() + 3);
            this.ReverseMon = false;
            this.Pressed = 1;
        }
        else if(Gdx.input.isKeyPressed(Input.Keys.LEFT)){
            //;
            this.ReverseMon = true;
            this.Pressed = 2;
        }
        else if(Gdx.input.isKeyPressed(Input.Keys.SPACE)){
            this.Pressed = 3;

        }


        if(this.Pressed == 1)
            this.setX(this.getX() + 3);

        else if (this.Pressed == 2)
            this.setX(this.getX() - 3);

        else
            this.setX(this.getX()+0);



        if(Gdx.input.isKeyPressed(Input.Keys.UP)){
            if(!this.ReverseMon && this.Pressed == 1){
                this.setX(this.getX() + 5);
            }
            if(this.ReverseMon && this.Pressed == 2){
                this.setX(this.getX() - 5);
            }
        }
        else if(Gdx.input.isKeyPressed(Input.Keys.DOWN)){
            if(!this.ReverseMon && this.Pressed == 1){
                this.setX(this.getX() - 1);
            }
            if(this.ReverseMon && this.Pressed == 2){
                this.setX(this.getX() + 1);
            }
        }


        // revote y tal
        if(this.getX() >= this.LimiteMapa - 40){
            this.Pressed = 2;
            this.ReverseMon = true;
        }
        else if(this.getX() <= 0){
            this.Pressed = 1;
            this.ReverseMon = false;
        }
        this.rec.setX(this.getX());
        this.rec.setY(this.getY());
    }

    @Override
    public void onColision(Esprite obj){
        if(this.rec.overlaps(obj.rec)) {
            if(this.Pressed == 1)
                this.Pressed = 2;
            else if (this.Pressed == 2)
                this.Pressed = 1;


            if((this.getY() > obj.getY() || this.getY() < obj.getY()) && this.getToco()) {

                obj.setPressedY(!obj.getPressedY());
                this.setToco(true);
            }
            else {
                obj.setPressedX(!obj.getPressedX());
                this.setToco(false);
            }

        }
    }

}
