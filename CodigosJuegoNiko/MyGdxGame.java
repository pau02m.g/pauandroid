package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;


import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.Random;


public class MyGdxGame extends ApplicationAdapter {
	//DEFINICIÓN VARIABLES
	int width; // ancho de la pantalla física.
	int height; // // alto de la pantalla física.
	OrthographicCamera cam; //La camera que usaremos.

	Jugador personaje; //sera nuestro personaje, esta clase contiene los métodos y atributos
	//Suelo suelo;




	//necesarios para que lo podamos mover, rotar y escalar de una manera mas fácil.
	SpriteBatch batch; //la clase encargada de dibujar en pantalla.


	Texture texturaPlayer;
	//Texture texturaSuelo;
	Texture texturaEnemigo;

	int anchopersona = 43;
	int altopersona = 64;

	int paso = 0;
	int cambiopaso = 0;
	int sentido = 0;

	float timerMain;
	float timerSpawn;
	float timerDisEnemigo;



	int puntuacion;

	BitmapFont puntuacionPorPantalla;

	//FIN DEFINICIÓN VARIABLES

	private Music music; //music = Gdx.audio.newMusic(Gdx.files.internal("sounds/fear.mp3"));

	Random rand = new Random();

	@Override
	public void create() {

		width = Gdx.graphics.getWidth();
		height = Gdx.graphics.getHeight();
		cam = new OrthographicCamera(width, height);
		cam.setToOrtho(false, width, height);
		batch = new SpriteBatch();

		//E:\2n DamVi\M8\JuegoM8UF1_Plataformas\android\assets
		texturaPlayer = new Texture(Gdx.files.internal("caminando.png")); // no va porque no quiere //Personaje.png
		//texturaSuelo = new Texture(Gdx.files.internal("Plataforma.jpg"));
		texturaEnemigo = new Texture(Gdx.files.internal("followE.png")); // aqui si funciona



		personaje = new Jugador(texturaPlayer);
		//suelo = new Suelo(texturaSuelo);



		personaje.setRegion(paso * anchopersona, altopersona * sentido, (paso + 1) * anchopersona, altopersona * (sentido + 1));  // Definimos que parte de la tetura queremos mostrar,



		//suelo.setPosition(-250, 30);
		//suelo.setScale(0.2f);


		/*
		MUsica
		Creamos el objeto Music y le decimos que queremos que cuando acabe vuelva a empezar con el metodo setLooping pasando el valor true
		 */

		/*
		music = Gdx.audio.newMusic(Gdx.files.internal("sounds/fear.mp3"));
		music.setLooping(true);
		music.play();
		*/


		// puntuacion

		puntuacion = 0;
		puntuacionPorPantalla = new BitmapFont();
	}

	@Override
	public void dispose() {

		batch.dispose();
		texturaPlayer.dispose();


		/*  musica: Para liberar el fichero y no dejarlo abierto cuando cerremos la aplicacion se usa el metodo dispose */
		//music.dispose();
	}

	@Override
	public void render() {

		// Logica juego

		// falsedad de movimento
		if(personaje.getTocado()){

		}
		cambiopaso = (cambiopaso + 1) % 10;
		if (cambiopaso == 0) {
			paso = (paso + 1) % 6;
			personaje.setRegion(paso * anchopersona, altopersona * sentido, anchopersona, altopersona * (sentido + 1));  // Definimos que parte de la tetura queremos mostrar
		}

		// Fin logica

		Gdx.gl20.glClearColor(1, 1, 1, 1);  // Definimos colo blanco de fondo
		Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT); //Limpiamos la pantalla

		batch.setProjectionMatrix(cam.combined);
		batch.begin();


		personaje.draw(batch);
		//suelo.draw(batch);

		String a = "Score: "+puntuacion;
		puntuacionPorPantalla.setColor(new Color(Color.BLACK));
		puntuacionPorPantalla.draw(batch, a, 10,450);


		personaje.MoverPersonaje();


		timerMain += Gdx.graphics.getDeltaTime();
		timerMain = personaje.Disparar(timerMain);

		timerSpawn += Gdx.graphics.getDeltaTime();
		timerSpawn = SpawnerEnemiges(timerSpawn);

		timerDisEnemigo += Gdx.graphics.getDeltaTime();


		for (Proyectil p : personaje.getListaProyectil()) {

			if(personaje.getListaEnemigos().size() > 0){
				p.SeguirEnemigo(personaje.getListaEnemigos().get(0));
			}
			p.draw(batch);
		}

		for(Enemigo e : personaje.getListaEnemigos()){
			if(e.getTipoEnemigo() == 0){ // 0 seguir // 1 estatico
				e.SeguirPlayer(personaje);
			}
			else{
				/*
				timerDisEnemigo = e.Contador_Propio(timerDisEnemigo);
				for(Proyectil p : e.getListaProyectil()){
					if(e.getX() == 0){
						p.DispararNormal(1);
					}
					else{
						p.DispararNormal(2);
					}
					p.draw(batch);
					personaje.onColisionProyectil(p);
				}

				 */
			}
			e.draw(batch);
		}

		//Disparo enemigo

		for(Enemigo e : personaje.getListaEnemigos()){
			timerDisEnemigo = e.Contador_Propio(timerDisEnemigo);
				for(Proyectil p : e.getListaProyectil()){
					if(e.getX() == 0){
						p.DispararNormal(1);
					}
					else{
						p.DispararNormal(2);
					}
					p.draw(batch);
					personaje.onColisionProyectil(p);
				}
		}







		for (Enemigo e : personaje.getListaEnemigos()) {
			for (Proyectil p : personaje.getListaProyectil()) {
				e.onColision(p);
			}
			personaje.onColisionEnemigo(e);
		}





		for (Enemigo e : personaje.getListaEnemigos()) {
			if(e.getTocado()){
				puntuacion = e.getPuntuacionEnemigo();
				personaje.getListaEnemigos().remove(e);
				break;
			}
		}

		for (Proyectil p : personaje.getListaProyectil()) {
			if(p.getTocado()){
				personaje.getListaProyectil().remove(p);
				break;
			}
		}












		batch.end(); //cerramos al dibujador
	}


	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}



	private float SpawnerEnemiges(float timerMain){
		if (timerMain > 1f) {

			//int TipoDeEnemigo = rand.nextInt(5)+1;
			int TipoDeEnemigo = 1;

			if(TipoDeEnemigo != 1)
			{

				Texture texturaP;
				texturaP = new Texture(Gdx.files.internal("followE.png"));
				texturaP.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

				Enemigo e = new Enemigo(texturaP, 50, 50, personaje);
				e.setTipoEnemigo(0);

				e.rec.setX(e.getX());
				e.rec.setY(e.getY());

				e.PosicionRandom();

				personaje.getListaEnemigos().add(e);
				return 0.0f;
			}
			else
			{


				Texture texturaP;
				texturaP = new Texture(Gdx.files.internal("enemigoDis.png"));
				texturaP.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

				Enemigo e = new Enemigo(texturaP, 50, 50, personaje);
				e.setTipoEnemigo(1);


				e.rec.setX(e.getX());
				e.rec.setY(e.getY());

				e.PosicionRandomEstatico();

				personaje.getListaEnemigos().add(e);
				return 0.0f;

			}


		}
		return timerMain;
	}


}
