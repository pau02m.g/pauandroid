package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class Proyectil extends Esprite {

    private boolean Tocado;

    private int tipo;

    public Proyectil(Texture texture, float ancho, float alto) {

        super(texture, ancho, alto);
        Tocado = false;

    }

    public void SeguirEnemigo(Enemigo j) {

        if (this.getX() != j.getX()) {
            if (this.getX() < j.getX()) {
                this.setX(this.getX() + 3);

            } else if (this.getX() > j.getX()) {
                this.setX(this.getX() - 3);

            }
        }
        if (this.getY() != j.getY()) {
            if (this.getY() < j.getY()) {
                this.setY(this.getY() + 3);
            } else if (this.getY() > j.getY()) {
                this.setY(this.getY() - 3);
            }
        }


        this.rec.setX(this.getX());
        this.rec.setY(this.getY());
        this.setTipo(0);
    }

    public void DispararNormal(int direccion){
        if(direccion == 1){ // 1 van a la derecha // 2 van a la izquierda
            this.setX(this.getX()+5);
        }
        else{
            this.setX(this.getX()-5);
        }
        this.rec.setX(this.getX());
        this.rec.setY(this.getY());

        this.setTipo(1);
    }


    public boolean getTocado(){
        return this.Tocado;
    }

    public void setTocado(boolean t){
        this.Tocado = t;
    }

    public void setTipo(int t){
        this.tipo = t;
    }
    public int getTipo(){
        return this.tipo;
    }

}
