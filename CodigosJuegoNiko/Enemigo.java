package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

import java.util.ArrayList;
import java.util.Random;

public class Enemigo extends Esprite{

    private boolean FlipEnemigo;
    Jugador player;

    static private int puntuacionEnemigo = 0;

    private int tipoEnemigo;

    private boolean tocado;

    private float timerPersonalEnemigo;

    private ArrayList<Proyectil> ListaProyectil_Enemigo = new ArrayList<Proyectil>();;

    public Enemigo(Texture texture, float ancho, float alto, Jugador j) {
        super(texture, ancho, alto);

        player = j;

        tocado = false;



    }




    // funciona perfe
    public void SeguirPlayer(Jugador j){

        if(this.getX() != j.getX()){
            if(this.getX() < j.getX()){
                this.setX(this.getX() + 0.5f);
                FlipEnemigo = false;
            }
            else if(this.getX() > j.getX()){
                this.setX(this.getX() - 0.5f);
                FlipEnemigo = true;
            }
        }
        if(this.getY() != j.getY()){
            if(this.getY() < j.getY()){
                this.setY(this.getY() + 0.5f);
            }
            else if(this.getY() > j.getY()){
                this.setY(this.getY() - 0.5f);
            }
        }


        if(FlipEnemigo == true){
            this.setFlip(FlipEnemigo,false);
        }
        else{
            this.setFlip(FlipEnemigo,false);
        }


        this.rec.setX(this.getX());
        this.rec.setY(this.getY());
    }

    public float DispararEnemigo(float timerMain){
        if (timerMain > 1f) {
            Texture texturaP;
            texturaP = new Texture(Gdx.files.internal("bola.png"));
            texturaP.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

            Proyectil p = new Proyectil(texturaP, 20, 20);
            //this.getListaProyectil().add(p);

            p.setX(this.getX());
            p.setY(this.getY());

            this.getListaProyectil().add(p);

            return 0.0f;
        }
        return timerMain;
    }

    public float Contador_Propio(float a){
        timerPersonalEnemigo = a;
        return DispararEnemigo(timerPersonalEnemigo);
    }


    public void onColision(Proyectil obj){

        if(this.rec.overlaps(obj.rec)) {
            if (obj.getClass().getName().equals("com.mygdx.game.Proyectil")) {
                //player.getListaProyectil().remove(player.getListaProyectil().indexOf(obj));


                //player.getListaEnemigos().remove(player.getListaEnemigos().indexOf(this));
                tocado = true;
                obj.setTocado(true);



                puntuacionEnemigo++;

            }
        }
    }


    public boolean getTocado(){
        return this.tocado;
    }
    public void setTocado(boolean t){
        this.tocado = t;
    }


    public void PosicionRandom(){
        Random random = new Random();
        int posicion = random.nextInt(4)+1;


        int width = Gdx.graphics.getWidth();
        int height = Gdx.graphics.getHeight();

        if(posicion == 1){
            this.setX(0);
            this.setY(random.nextInt(height));
        }
        else if(posicion == 2){
            this.setX(width);
            this.setY(random.nextInt(height));
        }

        else if(posicion == 3){
            this.setX(random.nextInt(width));
            this.setY(0);
        }
        else if(posicion == 4){
            this.setX(random.nextInt(width));
            this.setY(height);
        }
    }

    public void PosicionRandomEstatico(){
        Random random = new Random();
        int posicion = random.nextInt(2)+1;

        int width = Gdx.graphics.getWidth();
        int height = Gdx.graphics.getHeight();

        if(posicion == 1) // izquierda
        {
            this.setX(0);
            this.setY(random.nextInt(height));
        }
        else // derecha
        {
            this.setX(width-50);
            this.setY(random.nextInt(height));
            this.setFlip(true,false);
        }


        this.rec.setX(this.getX());
        this.rec.setY(this.getY());
    }




    public int getPuntuacionEnemigo(){return puntuacionEnemigo;}

    public int getTipoEnemigo(){return this.tipoEnemigo;}

    public void setTipoEnemigo(int i){this.tipoEnemigo = i;}

    public ArrayList<Proyectil> getListaProyectil(){
        return ListaProyectil_Enemigo;
    }

}
