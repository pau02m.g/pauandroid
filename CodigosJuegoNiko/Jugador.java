package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;

import java.util.ArrayList;

public class Jugador extends Esprite{

    private int LimiteMapa;


    private int SaltoNico;
    private int DesSaltoNico;
    private int VelocidadSaltoNico;
    private boolean Abajo;
    private boolean EstoySaltando;


    private ShapeRenderer shapeRenderer;
    static private boolean projectionMatrixSet;
    private SpriteBatch batch = new SpriteBatch();;


    private ArrayList<Enemigo> ListaEnemigos;
    private ArrayList<Proyectil> ListaProyectil;

    private boolean MeEstoyMoviemndo;
    private boolean tocado;

    public Jugador(Texture texture){
        super(texture, 46, 64);

        this.setPosition(270, 210);



        ListaEnemigos = new ArrayList<Enemigo>();
        ListaProyectil = new ArrayList<Proyectil>();
        tocado = false;


        //cosas nico
        SaltoNico = 0;
        DesSaltoNico = 0;
        VelocidadSaltoNico = 9;
        this.Abajo = false;
        this.EstoySaltando = false;


        // porsiacaso
        shapeRenderer = new ShapeRenderer();
        projectionMatrixSet = false;

    }








    public void MoverPersonaje(){

        if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)){
            this.setX(this.getX() + 3);
            MeEstoyMoviemndo = true;
            //this.flip(false,false);
        }
        else if(Gdx.input.isKeyPressed(Input.Keys.LEFT)){
            this.setX(this.getX() - 3);
            MeEstoyMoviemndo = true;
            //this.flip(true,false);
        }
        else if(Gdx.input.isKeyPressed(Input.Keys.DOWN)){
            this.setY(this.getY() - 3);
            MeEstoyMoviemndo = true;
        }
        else if(Gdx.input.isKeyPressed(Input.Keys.UP)){
            this.setY(this.getY() + 3);
            //Saltar();
            MeEstoyMoviemndo = true;
        }
        else{
            MeEstoyMoviemndo = false;
        }

        //HacerCosas();
        this.rec.setX(this.getX());
        this.rec.setY(this.getY());

    }






    public ArrayList<Enemigo> getListaEnemigos(){
        return ListaEnemigos;
    }



    public ArrayList<Proyectil> getListaProyectil(){
        return ListaProyectil;
    }


    public float Disparar(float timerMain){


        if(this.getMeEstoyMoviendo()){
            return 0.0f;
        }
        else {
            if (timerMain > 1f && this.getListaEnemigos().size() > 0 && this.getListaProyectil().size() < 3) {
                Texture texturaP;
                texturaP = new Texture(Gdx.files.internal("jabulani.png")); // aqui tambien funciona
                texturaP.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

                Proyectil p = new Proyectil(texturaP, 20, 20);
                Music SonidoDisparo; //music = Gdx.audio.newMusic(Gdx.files.internal("sounds/fear.mp3"));
                SonidoDisparo = Gdx.audio.newMusic(Gdx.files.internal("sounds/DisparoSound.mp3"));
                SonidoDisparo.play();
                this.getListaProyectil().add(p);

                p.setX(this.getX());
                p.setY(this.getY());
                p.setTipo(1); // 1 soy yo y 2 es enemigo

                return 0.0f;
            }
            return timerMain;
        }

    }



    public boolean getMeEstoyMoviendo(){
        return this.MeEstoyMoviemndo;
    }




    public void onColisionProyectil(Proyectil obj){

        if(this.rec.overlaps(obj.rec)) {
            System.out.println("Me a tocado algo");
            if (obj.getClass().getName().equals("com.mygdx.game.Proyectil") && obj.getTipo() == 1) {
                System.out.println("Me a tocado Proyectil");
                tocado = true;
                obj.setTocado(true);

            }
        }
    }

    public void onColisionEnemigo(Enemigo obj){

        if(this.rec.overlaps(obj.rec)) {
            System.out.println("Me a tocado algo");
            if (obj.getClass().getName().equals("com.mygdx.game.Enemigo")) {
                System.out.println("Me a tocado Enemigo");
                tocado = true;
                obj.setTocado(true);
            }
        }
    }


    public void finalDePartida(){
        if(this.tocado){

        }
    }


    public boolean getTocado(){return this.tocado;}


    ////////////////////////////////////////////////////////////
    // Cosas en desuso pero super utiles
    ////////////////////////////////////////////////////////////


    private void Saltar() {

        this.SaltoNico = 9;
        this.DesSaltoNico = 9;

    }

    private void HacerCosas(){

        // estoy saltando


        if(this.SaltoNico > 0){
            if(this.VelocidadSaltoNico%9 == 0){
                this.SaltoNico--;

                this.setY(this.getY() + 10);
                VelocidadSaltoNico = 9;
            }
            VelocidadSaltoNico--;
        }

        if(this.SaltoNico == 0){
            this.Abajo = true;
        }
        else {
            this.Abajo = false;
        }

        if(this.DesSaltoNico > 0 && Abajo){
            if(this.VelocidadSaltoNico%9 == 0){
                this.DesSaltoNico--;

                this.setY(this.getY() - 10);
                VelocidadSaltoNico = 9;
            }
            VelocidadSaltoNico--;
        }
    }



    /*
    public void draw(SpriteBatch batch){
        batch.end();
        if(!projectionMatrixSet){
            shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());
        }
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(Color.RED);
        shapeRenderer.rect(this.getX()*5, this.getY()*2, this.getWidth()*5, this.getHeight()*2);
        shapeRenderer.end();
        batch.begin();
    }
*/








}

